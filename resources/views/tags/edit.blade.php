

<div class="container">
  <!-- The Modal -->
  <div class="modal fade" id="edit">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Edit Tag</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
                {!! Form::model($tag ,['route' => ['tags.update',$tag->id],'method'=>'PUT']) !!}
                {{ Form::label('name', ' Name')}} 
                {{ Form::text('name',null, array('class'=> 'form-control', 'required'=>'', 'id'=> 'name'))}}

                {{ Form::text('id',null, array('class'=> 'form-control', 'required'=>'', 'id'=> 'id', 'hidden'=>''))}}
        
                {{form::submit('Save Changes', array('class'=>'btn btn-success btn-lg btn-block mt-3'))}}
        </div>
        
      </div>
    </div>
  </div>
  
</div>

<script>
        $('#edit').on('show.bs.modal', function (event) {
         var button = $(event.relatedTarget) 
         var id = button.data('id')
         var name = button.data('name')
         
         var modal = $(this)
         modal.find('.modal-body #id').val(id)
         modal.find('.modal-body #name').val(name)
         })
</script>


