@extends('admin.dashboard')

@section('content')

 
  <div class="top_content">
    <h3>Tags</h3>
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Create</button>
  </div>
 
  
      
<div class="c-table">
     
        <table class="table table-hover" id="table">
            <thead class="c-head">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
                @foreach ($tags as $tag)
                <tr>
                    <td>{{$tag->id}}</td>
                    <td>{{$tag->name}}</td>
                    <td>
                      <div class="btn-group" role="group" aria-label="Basic example">
                      <button type="button" data-id="{{$tag->id}}" data-name="{{$tag->name}}" class="btn btn-primary mr-1" data-toggle="modal" data-target="#edit">Edit</button>
                      {!!Form::open(['route'=>['tags.destroy',$tag->id],'method'=>'DELETE'])!!}
                      {{ Form::submit('Delete',['class'=>'btn btn-danger ']) }}
                      {!!Form::close()!!}
                      </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
</div>
  
        @include('tags.create')
        @include('tags.edit')

@endsection