<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>   

</head>
<body>

  <div class="container">
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Create</button>
  
    <div class="row">
      <div class="col-md-8 m-auto">
        <table class="table">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Name</th>
              <th scope="col">Action</th>
              <th scope="col"></th>
            </tr>
          </thead>
          <tbody>
              @foreach ($tags as $tag)
              <tr>
                  <td>{{$tag->id}}</td>
                  <td>{{$tag->name}}</td>
                  <td><button type="button" data-id="{{$tag->id}}" data-name="{{$tag->name}}" class="btn btn-primary d-inline" data-toggle="modal" data-target="#edit">Edit</button></td>
                   <td>
                    {!!Form::open(['route'=>['tags.destroy',$tag->id],'method'=>'DELETE'])!!}
                    {{ Form::submit('Delete',['class'=>'btn btn-danger']) }}
                    {!!Form::close()!!}
                  </td>
              </tr>
              @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>


  
  

  @include('tags.create');
  @include('tags.edit');

  
      
      
</body>
</html>