<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>home</title>

    <link rel="stylesheet" href="/css/main.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
</head>
<body>
    <nav class="desk_nav">
        <div class="nav_container desk">
            <div class="logo">
                <img class="logo" src="/images/logo.png" alt="">
            </div>
            <div>
                <a  class="nav_items" href="">articles</a>
            </div>
            <div>
                <a class="nav_items" href="">Quran</a>
            </div>
            <div>
                <a class="nav_items" href="">hadith</a>
            </div>
            <div>
                <a class="nav_items" href="">dua</a>
            </div>
            <div>
                <a  class="nav_items"href="">videos</a>
            </div>
            <div>
                <a class="nav_items" href="">about us</a>
            </div>
            <div class="q_container">
               {!!Form::open()!!}
               {{Form::text('q',null,array('class' => 'q' ))}}
                <button type="button" class="q_btn">
                    <img class="q_logo" src="/images/q.png">
                </button>
                {!!Form::close()!!}
            </div>

        </div>
    </nav>

    <nav class="mobile_nav">
        <div class="mobile_nav_head">
            <div>
                <button onclick="openFunction()" class="open_menu"><img src="/images/menu.png" alt=""></button>
                <button onclick="closeFunction()" class="close_menu"><img src="/images/close.png" alt=""></button>
            </div>
    
            <div>
                <img class="q_logo" src="/images/q.png">
            </div>
        </div>
        
       




        <div id="menu" class="mobile_nav_container menu_display">
            <div>
                <a  class="nav_items" href="">articles</a>
            </div>
            <div>
                <a class="nav_items" href="">Quran</a>
            </div>
            <div>
                <a class="nav_items" href="">hadith</a>
            </div>
            <div>
                <a class="nav_items" href="">dua</a>
            </div>
            <div>
                <a  class="nav_items"href="">videos</a>
            </div>
            <div>
                <a class="nav_items" href="">about us</a>
            </div>
        </div>

    </nav>

    
    @yield('content')
     
    <footer>
        <div class="foot_container">
            <div class="foot_content">
                <img class="foot_img" src="/images/logo_white.png" alt="">
                <p>Copyright 2020 Al-Sirat</p>
            </div>
            
        </div>
    </footer>

    <script>
        function openFunction() {
            $('.mobile_nav_container').show("slow");
            $('.open_menu').hide();
            $('.close_menu').show();

            
        }

        function closeFunction() {
            $('.mobile_nav_container').hide();
            $('.close_menu').hide();
            $('.open_menu').show();

        }


        </script>


<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
</body>
</html>