<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
  

    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.tiny.cloud/1/no-api/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
{!!Html::style('css/select2.css')!!}
  

   

    <title>Document</title>
</head>
<body>
    <div class="body">
        <div class="side_nav">
            <div class="brand">
            </div>
            <ul class="dbul">
                <li class="dbli">Dashboard</a></li>
                <li class="dbli"><a class="dba" href="">Posts</a></li>
                <li class="dbli"> <a class="dba href="">Categories</a></li>
                <li class="dbli"> <a class="dba href="">Tags</a></li>
            </ul>  
        </div>
        <div class="content_area">
            <div class="top_nav">
    
            </div>
            <div class="content">
                @yield('content')
            </div>
        </div>  
    </div>
    
</body>
</html>