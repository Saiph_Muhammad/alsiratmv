<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
  <script src="https://cdn.tiny.cloud/1/no-api/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
  <script>tinymce.init({
    selector:'textarea',
    menubar:false,
  });</script>
</head>
<body>
  <div class="container mt-5">
    <div class="row">
      <div class="col-md-8 offset-2">
        <h2>Edit Post</h2>
        <hr>
        {!! Form::model($post ,['route' => ['posts.update',$post->id],'method'=>'PUT']) !!}
        {{ Form::label('title', ' Title')}} 
        {{ Form::text('title',null, array('class'=> 'form-control', 'required'=>''))}}

        {{ Form::label('slug', ' Slug')}} 
        {{ Form::text('slug',null, array('class'=> 'form-control', 'required'=>''))}}


        {{form::label('body','Post Body')}}
        {{form::textarea('body',null, array('class'=>'form-control', 'required'=>''))}}

        {{form::submit('Save Changes', array('class'=>'btn btn-success btn-lg btn-block mt-3'))}}
        
      </div>
    </div>

  </div>

  
  




  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
</body>
</html>