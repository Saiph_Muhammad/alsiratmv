@extends('admin.dashboard')

@section('content')
<div class="row">
  <div class="col-md-8">
    <div class="cmodal-main">
      <h3>{{ $post->title }}</h3>
      <p>{!! $post->body !!}</p>
    </div>
  </div>
  
  <div class="col-md-4">
    <div class="cmodal">
      <ul class="detail">
      <li>Slug<a href="http://127.0.0.1:8000/article/{{$post->slug}}"> : {{$post->slug}}</a></li>
      <li>Created at : {{$post->created_at->format('d M Y')}}</li>
      <li>Category : {{$post->category->name}}</li>
      <li>Tags :
        @foreach($post->tags as $tag)
        <span class="tags">{{$tag->name}}</span>
        @endforeach
      </li>
      </ul>
     
    
      <div class="btn-group" role="group" aria-label="Basic example">
        {!! Html::linkroute('posts.index','View' ,'',array('class'=>'btn btn-success'))  !!}
      <button type="button" data-id="{{$post->id}}" data-name="{{$post->name}}" class="btn btn-primary mr-1 ml-1" data-toggle="modal" data-target="#edit">Edit</button>
      {!!Form::open(['route'=>['posts.destroy',$post->id],'method'=>'DELETE'])!!}
      {{ Form::submit('Delete',['class'=>'btn btn-danger ']) }}
      {!!Form::close()!!}
      </div>
   
  </div>
</div>

@endsection

   
