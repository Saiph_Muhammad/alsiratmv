@extends('admin.dashboard')

@section('content')

      {!! Form::open(['route' => 'posts.store', 'data-parsley-validate'=>'', 'files'=>true]) !!}
      <div class="row">
        <div class="col-md-8">
          <div class="cmodal">
            <h5>Create New Post</h5>
            {{ Form::label('title', ' Title')}} 
            {{ Form::text('title',null, array('class'=> 'form-control', 'required'=>''))}}
  
            {{form::label('body','Post Body')}}
            {{form::textarea('body',null, array('class'=>'form-control'))}}
          </div>
          
        </div>
        <div class="col-md-4">
          <div class="cmodal">
            {{ Form::label('category_id', ' Category')}} 
            <select name="category_id" class="form-control" >
              @foreach ($categories as $category)
              <option value="{{$category->id}}">{{$category->name}}</option>
              @endforeach
            </select>
      
            {{ Form::label('tags[]', ' Tags')}} 
            <select name="tags[]" class="form-control select-multiple" multiple="multiple">
              @foreach ($tags as $tag)
              <option value="{{$tag->id}}">{{$tag->name}}</option>
              @endforeach
            </select>
            
            {{form::label('featured_image','Featured Image')}}
            {{form::file('featured_image', array('class'=>'form-control'))}}


            <div class="btn-group" role="group" aria-label="Basic example">
            {{form::submit('Publish Post', array('class'=>'btn btn-success t-btn mt-3'))}}
            {{form::submit('Save Draft', array('class'=>'btn btn-primary t-btn mt-3 ml-2'))}}
            </div>
            {!! Form::close() !!}
          
          </div>
         
       

        </div>
      </div>
     
       
     
          
     
      

     


      {{-- {{ Form::label('slug', ' slug')}} 
      {{ Form::text('slug',null, array('class'=> 'form-control', 'required'=>''))}} --}}

     
  





<script type="text/javascript" src="{{ asset('js/select2.min.js') }}"></script>

<script type="text/javascript">
  $(document).ready(function() {
  $('.select-multiple').select2();
  theme: "primary"
   
  tinymce.init({
  selector:'textarea',
  menubar:false,
  });
    
   });
</script>
 
    
@endsection

  



