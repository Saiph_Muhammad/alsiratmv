@extends('admin.dashboard')

@section('content')

 
  <div class="top_content">
    <h3>Posts</h3>
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Create</button>
  </div>
 
  
      
<div class="c-table">
     
        <table class="table table-hover" id="table">
            <thead class="c-head">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Title</th>
                {{-- <th scope="col">Category</th> --}}
                <th scope="col">Body</th>
                <th scope="col">Created</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
                @foreach ($posts as $post)
                <tr>
                  <td>{{$post->id}}</td>
                  <td>{{$post->title}}</td>
                  {{-- <td>{{$post->Category['name']}}</td> --}}
                  <td>{{ substr(strip_tags($post->body),0,200) }}{{strlen(strip_tags($post->body)) > 200 ? "..." :"" }}</td>
                  <td style="white-space: nowrap;">{{$post->created_at->format('d M Y')}}</td>
                    <td>
                      <div class="btn-group" role="group" aria-label="Basic example">
                        {!! Html::linkroute('posts.show','View' ,array($post->id),array('class'=>'btn btn-success t-btn'))  !!}
                      <button type="button" data-id="{{$post->id}}" data-name="{{$post->name}}" class="btn btn-primary mr-1 ml-1 t-btn" data-toggle="modal" data-target="#edit">Edit</button>
                      {!!Form::open(['route'=>['posts.destroy',$post->id],'method'=>'DELETE'])!!}
                      {{ Form::submit('Delete',['class'=>'btn btn-danger t-btn ']) }}
                      {!!Form::close()!!}
                      </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{ $posts->links() }}
</div>
  

@endsection