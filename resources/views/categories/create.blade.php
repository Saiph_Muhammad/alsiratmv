
<div class="container">
    <!-- The Modal -->
    <div class="modal fade" id="myModal">
      <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
        
          <!-- Modal Header -->
          <div class="modal-header">
            <h5 class="modal-title">Create Category</h5>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          
          <!-- Modal body -->
          <div class="modal-body">
              {!! Form::open(['route' => 'categories.store', 'data-parsley-validate'=>'']) !!}
              {{ Form::label('name', ' Name')}} 
              {{ Form::text('name',null, array('class'=> 'form-control', 'required'=>''))}}
              {{form::submit('Save', array('class'=>'btn btn-success btn-lg btn-block mt-3'))}}
              {!!Form::close()!!}
          </div>
          
        </div>
      </div>
    </div>
    
  </div>
  
  