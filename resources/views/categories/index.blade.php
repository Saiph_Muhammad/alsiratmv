@extends('admin.dashboard')

@section('content')

 
  <div class="top_content">
    <h3>Categories</h3>
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Create</button>
  </div>
 
  
      
<div class="c-table">
     
        <table class="table table-hover" id="table">
            <thead class="c-head">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
                @foreach ($categories as $category)
                <tr>
                    <td>{{$category->id}}</td>
                    <td>{{$category->name}}</td>
                    <td>
                      <div class="btn-group" role="group" aria-label="Basic example">
                      <button type="button" data-id="{{$category->id}}" data-name="{{$category->name}}" class="btn btn-primary mr-1" data-toggle="modal" data-target="#edit">Edit</button>
                      {!!Form::open(['route'=>['categories.destroy',$category->id],'method'=>'DELETE'])!!}
                      {{ Form::submit('Delete',['class'=>'btn btn-danger ']) }}
                      {!!Form::close()!!}
                      </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
</div>
  
        @include('categories.create')
        @include('categories.edit')

@endsection