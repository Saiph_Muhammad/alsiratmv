@extends('admin.main')
@section('content')

<div class="container tspacer">
  <div class="single_view">
    <h1>{{ $post->title }}</h1>
    <h6>by sheihk abdulla azzam on {{ $post->created_at->format('d M Y') }}</h6>
    <img class="img-fluid mt-4" src="/images/img.jpg" alt="">
    <p>{!! $post->body !!}</p>
    <div class="tag_container">
      <hr>
      @foreach($post->tags as $tag)
      <span class="tags">{{$tag->name}}</span>
      @endforeach
    </div>
  </div>
  
</div>

@endsection


  
   

