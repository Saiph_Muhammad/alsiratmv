<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Post;
use App\Category;
use App\Tag;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        
        $tags = Tag::all();
        $posts = Post::paginate(4);
        return view('posts.index')->withPosts($posts)->withTags($tags);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $tags = Tag::all();
        $categories = Category::all();
        return view('posts.create')->withCategories($categories)->withTags($tags);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      
            //
        $validator = Validator::make($request->all(), [
            'title' => 'required|unique:posts|max:255',
            'body' => 'required',
            'slug' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('posts/create')
                        ->withErrors($validator)
                        ->withInput();
        }

         $post = new Post;
         $post -> title = $request->title;
         $post -> body = $request ->body;

         $slug = $request->title;
         // replace non letter or digits by -
         $slug = preg_replace('~[^\pL\d]+~u', '-', $slug);
         // transliterate
        $slug = iconv('utf-8', 'us-ascii//TRANSLIT', $slug);
         // remove unwanted characters
         $slug = preg_replace('~[^-\w]+~', '', $slug);
         // trim
        $slug = trim($slug, '-');
         // remove duplicate -
        $slug = preg_replace('~-+~', '-', $slug);
         // lowercase
        $slug = strtolower($slug);
        $post-> slug = $slug;
         
        $post -> category_id = $request ->category_id;
        
        if ($request->hasfile('featured_image')) {
            $image = $request->file('featured_image');
            $filename = time() . '.' . $image->getClientOriginalExtention();
            $location = public_path('images/' . $filename);
            image::make($image)->resize();
        } else {
            # code...
        }
        

        $post -> save();

        $post->tags()->sync($request->tags, false);

        return redirect()->route('posts.show', $post->id);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $tags = Tag::all();
        $post = Post::find($id);
        return view('posts.show')->withPost($post)->withTags($tags);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $post = Post::find($id);
        return view('posts.edit')->withPost($post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = Validator::make($request->all(), [
            'title' => 'required|unique:posts|max:255',
            'slug' => 'required',
            'body' => 'required',
        ]);

         $post = Post::find($id);
         $post -> title = $request->input('title');
         $post -> body = $request ->input('body');
         $post -> slug = $request ->input('slug');
         $post -> save();

         return redirect()->route('posts.show', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $post = Post::find($id);
        $post->tags()->detach();
        $post -> delete();

        return redirect()->route('posts.index');
    }

   
}
